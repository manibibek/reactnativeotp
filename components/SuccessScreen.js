import React from 'react';
import { StyleSheet, Button, Text, View } from 'react-native';


class SuccessScreen extends React.Component {
    
    render() {
        return (
            <View style={[styles.container, {
                flexDirection: "column"
            }]}>
                <View style={{ padding: 30, backgroundColor: "white", alignItems: "center" }}>
                <Text style={[styles.header]}>Verification Succesfull</Text>
                </View>
                <Button id="submitBtn" title="Go Back" onPress={() => this.props.navigation.goBack() } />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        fontSize: 20,
    },
    container: {
        flex: 1,
        padding: 20,
      },
});

export default SuccessScreen;