import React from 'react';
import { StyleSheet, Button, Text, View, Alert, TextInput, Clipboard, Image } from 'react-native';


class OtpScreen extends React.Component {
    state = {
        otp: [],
        submitBtnDisabled: true,
        fields: 6,
        verified: false,
        showOtpPage: true
    };
    otpTextInput = [];

    static navigationOptions = () => ({
        title: 'Artists',
        headerTintColor: '#fff',
        headerStyle: {
          backgroundColor: '#FF3D00'
        }
      });

    
    componentDidMount() {
        this.otpTextInput[0].focus();
    }

    renderOtpFields() {
        const inputs = Array.from(Array(this.state.fields).keys());
        var fields = [];
        inputs.forEach(item => {
            fields.push(<TextInput
                style={styles.input}
                value={this.state.otp[item]}
                onChangeText={key => this.focusNext(item, key)}
                ref={ref => this.otpTextInput[item] = ref}
                onKeyPress={e => this.focusPrevious(e.nativeEvent.key, item)}
                keyboardType="numeric"
                textContentType="oneTimeCode"
                key={item}
            />)
        })

        return fields;
    }

    focusPrevious = (key, index) => {
        if (key === 'Backspace' && index !== 0) {
            this.otpTextInput[index - 1].focus();
        }
        this.updateOtp(index, null);
    }

    updateOtp = (index, value) => {
        let otp = this.state.otp;
        otp[index] = value;
        const otpLength = otp.filter(n => n).length;
        this.setState({ otp });
        this.submitButtonEvent(otpLength);
    }

    focusNext = async (index, value) => {
        if (index < this.otpTextInput.length - 1 && value) {
            this.otpTextInput[index + 1].focus();
        }
        if (index === this.otpTextInput.length - 1) {
            this.otpTextInput[index].blur();
        }
        
        const copiedContent = await Clipboard.getString();
        if (value.includes(copiedContent)) {
            this.handleOnPaste(copiedContent);
        } else {
            value = value.replace(/[^0-9]/g, '');
            if (value.toString().length > 1) {
                this.otpTextInput[index].focus();
                return;
            }
            this.updateOtp(index, value);
        }
    }

    handleOnPaste = (copiedContent) => {
        let otpArray = copiedContent.split('');
        otpArray = otpArray.filter(Number);
        if (otpArray.length == this.state.fields) {
            otpArray.forEach((item, index) => {
                this.updateOtp(index, item);
            })
        } else {
            this.otpTextInput[0].clear();
            this.otpTextInput[0].focus();
        }
    }

    submitButtonEvent = (otpLength) => {
        if (otpLength == this.state.fields) {
            this.setState({ 'submitBtnDisabled': false });
        } else {
            this.setState({ 'submitBtnDisabled': true });
        }
    }

     submit(navigation) {
        let data = {
            method: 'POST',
            body: JSON.stringify({
                otp: this.state.otp.join('')
            }),
            headers: {
                'Accept':       'application/json',
                'Content-Type': 'application/json',
            }
        };
         
         fetch('https://pacific-ravine-20733.herokuapp.com/submitOtp', data).then((response) => response.json())
             .then((responseJson) => {
                 if (responseJson.status == 'success') {
                    navigation.navigate('Success')
                 } else {
                     Alert.alert('Verification Failed. Please try again');
                 }
         }).catch((error) => {
            console.log(error);
        })
    }


    render() {
        return (
            <View style={[styles.container, {
                flexDirection: "column", padding: 30
            }]}>
                <View style={{ padding: 30, alignItems: "center" }}>
                <Text style={[styles.header]}>Verification Code</Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    {this.renderOtpFields()}
                </View>
                <View style={{ marginTop: 20}}>
                    <Button id="submitBtn" disabled={this.state.submitBtnDisabled} title="Submit" onPress={() => this.submit(this.props.navigation)} />
                </View>
                </View>
        );
    }
}

const styles = StyleSheet.create({
    gridPad: { padding: 40 },
    txtMargin: { margin: 3 },
    inputRadius: { textAlign: 'center' },
    input: {
        height: 40,
        margin: 5,
        borderWidth: 1,
        padding: 10,
        display: 'flex',
        borderRadius: 5
    },
    header: {
        fontSize: 20,
    },
    container: {
        flex: 1,
        padding: 20,
      },
});

export default OtpScreen;