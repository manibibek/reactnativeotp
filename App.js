import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Button, Text, View, Alert, TextInput } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import OtpScreen from './components/OtpScreen';
import SuccessScreen from './components/SuccessScreen';

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Home" component={OtpScreen}/>
          <Stack.Screen name="Success" component={SuccessScreen} />
      </Stack.Navigator>
      <StatusBar style="default" />
    </NavigationContainer>
  );
}